""" Contains the view logic for views that don't fit in a specific app. For
example, the home page.
"""

import logging

from django.shortcuts import render

logger = logging.getLogger(__name__)


def home(request):
    """ Renders the home page """
    logger.info("Rendering the home page")
    return render(request, "home.html")
