""" Form classes for the summoner_profile app """
from django import forms

from registration.forms import RegistrationForm


class LinkSummonerForm(forms.Form):

    """Form used to link a summoner name to a user account"""
    summoner_name = forms.CharField(label='Summoner name', max_length=50)


class SummonerRegistrationForm(RegistrationForm):

    """Form for registering a user account, including summoner name """
    summoner_name = forms.CharField(label="Summoner name", max_length=50)


class UserSearchForm(forms.Form):

    """Form for searching for a user"""
    name_field = forms.CharField(
        label="Account or summoner name", max_length=50)
