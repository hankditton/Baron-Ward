from django.contrib.auth.models import User
from django.test import TestCase

from summoner_profile.models import SummonerProfile


# Create your tests here.
class RegistrationTestCase(TestCase):

    def test_summoner_registration(self):
        response = self.client.post("/accounts/register",
                                    {'username': 'user',
                                     'email': 'test@email.com',
                                     'password1': 'pw',
                                     'password2': 'pw', 'summoner_name':
                                     'rakalakalili'})
        testUser = User.objects.get(username='user')
        self.assertIsNotNone(testUser, 'User account should not be null')
        self.assertEqual('user', testUser.username)
