"""URL configuration for account profiles
"""

from django.conf.urls import url
from django.contrib.auth.decorators import login_required
from django.contrib.auth.views import login

from summoner_profile.views import SummonerRegistrationView, SummonerLinkView,\
    UserSearchView

from . import views


urlpatterns = [
    url(r'^search/$', login_required(UserSearchView.as_view())),
    url(r'^link/$', login_required(SummonerLinkView.as_view())),
    url(r'register/$', SummonerRegistrationView.as_view()),
    url(r'login/$', login, {'template_name': 'login.html'}),
    url(r'^profile/$', views.your_profile),
    url(r'^profile/(?P<account_name>[\w]{1,20})/$', views.user_profile)
]
