"""View functions that render pages for viewing user profiles and information
"""

import logging
import random
import string

from cassiopeia import riotapi
from django.conf import settings
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.shortcuts import redirect
from django.shortcuts import render
from django.views.generic.edit import FormView
from registration.backends.default.views import RegistrationView

from summoner_profile.forms import LinkSummonerForm, SummonerRegistrationForm,\
    UserSearchForm
from teams.models import Team

from .models import SummonerProfile


logger = logging.getLogger(__name__)
riotapi.set_region("NA")
riotapi.set_api_key(settings.RIOT_API_KEY)


class SummonerRegistrationView(RegistrationView):

    """A class view for registering users, overriding RegistrationView to
    additionally create a SummonerProfile
    """
    form_class = SummonerRegistrationForm
    template_name = "registration.html"

    def register(self, request, form):
        """Registers a user via RegistrationView.user, then creates a summoner
        profile for that user and saves it.
        """
        logger.info("Creating a new user account")
        user = RegistrationView.register(self, request, form)
        logger.info(
            "User %s has been created, now creating a summoner profile", user)
        summoner_name = form.cleaned_data['summoner_name']
        summoner = SummonerProfile(
            user=user, summoner_name=summoner_name,
            link_password=id_generator(8), verified_summoner=False)
        summoner.save()


class SummonerLinkView(FormView):

    """A class view for handling linking an account with a League of Legends
    account
    """
    template_name = 'link.html'
    form_class = LinkSummonerForm
    success_url = '/accounts/profile'

    def form_valid(self, form):
        """Grabs the summoner_name from the form and checks if any rune pages
        of that summoner have a name equal to the user's link_password. If
        successfully verified, sets the user's summoner_id and sets
        verified_summoner = True
        """
        summoner_name = form.cleaned_data['summoner_name']
        logger.info("Attempting to link summoner name %s with user account %s",
                    summoner_name, self.request.user)
        summoner = riotapi.get_summoner_by_name(summoner_name)
        logger.info("Found summoner %s", summoner)
        rune_pages = riotapi.get_rune_pages(summoner)
        logger.info("Found rune pages for summoner: %s", rune_pages)
        if self.request.user.summonerprofile.link_password not in [
                rune_page.name for rune_page in rune_pages]:
            logger.info("Did not find the summoner's link code (%s) in the"
                        " rune pages",
                        self.request.user.summonerprofile.link_password)
            messages.error(
                self.request, "Could not find a rune page that matched your "
                "link code on summoner '%s'" % summoner_name)
            return self.get(self.request)
        else:
            logger.info("Link password found, marking summoner as verified")
            self.request.user.summonerprofile.summoner_name = summoner_name
            self.request.user.summonerprofile.verified_summoner = True
            self.request.user.summonerprofile.summoner_id = summoner.id
            self.request.user.summonerprofile.save()
            messages.success(
                self.request, "Successfully linked your account to summoner "
                "account '%s'!" % summoner_name)
            return redirect('/accounts/profile')


class UserSearchView(FormView):

    """ FormView to handle the user search view. Finds users whose user account
    name or summoner name contain the searched for text
    """
    template_name = 'userSearch.html'
    form_class = UserSearchForm

    def form_valid(self, form):
        """Lookup all users whose account name or verified summoner name
        contains the search term, then renders the search page with the users
        found
        """
        search_name = form.cleaned_data['name_field']
        logger.info("Searching for users with name containing %s", search_name)
        users = User.objects.filter(
            Q(username__contains=search_name) |
            Q(summonerprofile__summoner_name__contains=search_name,
              summonerprofile__verified_summoner=True
              ))
        logger.info("Found the following users %s", users)
        return render(self.request, "userSearch.html", {'form': form, 'users':
                                                        users})


def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    """Small helper method to generate a random string of the passed in length
    using the passed in characters
    """
    return ''.join(random.choice(chars) for _ in range(size))


def user_profile(request, account_name):
    """Renders the public profile of a user """
    logger.info("Rendering public user profile for user %s", account_name)
    user = User.objects.get(username=account_name)
    teams = Team.objects.filter(
        teammembership__user=user, teammembership__active=True)
    logger.info(
        "Found user %s, who is on the following teams: %s", user, teams)
    return render(request, 'publicProfile.html', {'user': user,
                                                  'teams': teams})


@login_required
def your_profile(request):
    """Renders the page for a user's own profile when logged in"""
    logger.info("Rendering profile of signed in user %s", request.user)
    if request.user.summonerprofile.verified_summoner:
        teams = Team.objects.filter(members=request.user)
        logger.info("Found the following teams for the user %s", teams)
        return render(request, 'profile.html', {'teams': teams})
    else:
        logger.info("User %s has not linked a summoner account yet, redirect"
                    " to link page", request.user)
        messages.warning(
            request, "You haven't linked to a summoner account yet, please do"
            " so now")
        return redirect('/accounts/link')
