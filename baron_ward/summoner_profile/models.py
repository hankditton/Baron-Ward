""" Models for the summoner profile app, containing all league of legends
information about users
"""

from django.contrib.auth.models import User
from django.db import models


class SummonerProfile(models.Model):

    """ Holds information about a user's league of legend account. """
    user = models.OneToOneField(User)
    summoner_id = models.IntegerField(blank=True, null=True)
    summoner_name = models.CharField(max_length=50)
    link_password = models.CharField(max_length=8)
    verified_summoner = models.BooleanField()

    def __str__(self):
        return "Summoner Name: %s, Verified %s" % (self.summoner_name,
                                                   self.verified_summoner)


class Rating(models.Model):

    """ Holds rating information """
    TIER_CHOICES = (
        ("U", "Unranked"),
        ("B", "Bronze"),
        ("G", "Gold"),
        ("P", "Platinum"),
        ("D", "Diamond"),
        ("M", "Master"),
        ("C", "Challenger")
    )
    tier = models.CharField(max_length=1, choices=TIER_CHOICES)
    division = models.IntegerField()
    lp = models.IntegerField()
    summoner = models.OneToOneField(SummonerProfile)

    def __str__(self):
        return "Tier: %s, Division: %s, LP: %s" % (self.tier, self.division,
                                                   self.lp)
