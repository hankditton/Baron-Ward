""" Sets up the admin page for users and summoner profiles """

from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User
from nested_inline.admin import NestedStackedInline, NestedModelAdmin

from summoner_profile.models import Rating
from summoner_profile.models import SummonerProfile


class RatingInline(NestedStackedInline):

    """ Form to edit Rating while editing a SummonerProfile """
    model = Rating


class SummonerProfileInline(NestedStackedInline):

    """ Form to edit a SummonerProfile while editing a UserProfile """
    model = SummonerProfile
    can_delete = False
    inlines = (RatingInline, )


class SummonerProfileAdmin(NestedModelAdmin):

    """ Admin form to edit SummonerProfile, adding an inline Rating"""
    inlines = (RatingInline, )


class UserAdmin(NestedModelAdmin):

    """ Admin form to edit user, adding an inline SummonerProfile editor """
    inlines = (SummonerProfileInline, )


admin.site.unregister(User)
admin.site.register(User, UserAdmin)
admin.site.register(SummonerProfile, SummonerProfileAdmin)
admin.site.register(Rating)
