# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Rating',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('tier', models.CharField(max_length=1, choices=[('U', 'Unranked'), ('B', 'Bronze'), ('G', 'Gold'), ('P', 'Platinum'), ('D', 'Diamond'), ('M', 'Master'), ('C', 'Challenger')])),
                ('division', models.IntegerField()),
                ('lp', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='SummonerProfile',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('sumoner_id', models.IntegerField(null=True, blank=True)),
                ('summoner_name', models.CharField(max_length=50)),
                ('link_password', models.CharField(max_length=8)),
                ('verified_summoner', models.BooleanField()),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='rating',
            name='summoner',
            field=models.OneToOneField(to='summoner_profile.SummonerProfile'),
        ),
    ]
