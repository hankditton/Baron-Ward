# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('summoner_profile', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='summonerprofile',
            old_name='sumoner_id',
            new_name='summoner_id',
        ),
    ]
