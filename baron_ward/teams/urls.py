"""URL configuration for teams
"""

from django.conf.urls import url
from django.contrib.auth.decorators import login_required

from teams.views import CreateTeamView, team_page, join_team, quit_team,\
    kick_player, promote_captain, TeamSearchView

urlpatterns = [
    url(r'^create/$', login_required(CreateTeamView.as_view())),
    url(r'^search/$', login_required(TeamSearchView.as_view())),
    url(r'^(?P<team_name>.{1,50})/join/$', join_team),
    url(r'^(?P<team_name>.{1,50})/quit/$', quit_team),
    url(r'^(?P<team_name>.{1,50})/kick/(?P<username>.{1,50})/$', kick_player),
    url(r'^(?P<team_name>.{1,50})/promote/(?P<username>.{1,50})/$',
        promote_captain),
    url(r'^(?P<team_name>.{1,50})/$', team_page),
]
