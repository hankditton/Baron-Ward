""" Views for creating and managing teams and team memberships
"""
import logging

from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.db.models.query_utils import Q
from django.shortcuts import redirect, render
from django.views.generic.edit import FormView

from teams.forms import CreateTeamForm, JoinTeamForm, TeamSearchForm
from teams.models import Team, TeamMembership

logger = logging.getLogger(__name__)


class CreateTeamView(FormView):

    """A class view for handling creating a team. Fails if the team name
    already exists
    """
    template_name = 'createTeam.html'
    form_class = CreateTeamForm

    def form_valid(self, form):
        """Validates that the team name does not exist, then creates the team.
        """
        logging.info("Got request to create a team: %s" % form.cleaned_data)
        team_name = form.cleaned_data['team_name']
        team_tag = form.cleaned_data['team_tag']
        join_password = form.cleaned_data['join_password']
        if not Team.objects.filter(name=team_name).exists():
            logger.info("Team doesn't exist yet, proceeding to create team")
            team = Team(name=team_name, tag=team_tag,
                        join_password=join_password)
            team.save()
            logger.info(
                "Team %s created, adding member as captain: %s", team,
                self.request.user)
            membership = TeamMembership(team=team, user=self.request.user,
                                        is_captain=True)
            membership.save()
            return redirect('/teams/%s' % team_name)
        else:
            logger.info("Team name %s is already used", team_name)
            messages.error(self.request, 'Team name %s already exists'
                           % team_name)
            return self.get(self.request)


class TeamSearchView(FormView):

    """ FormView to handle the team search view. Finds teams whose name or tag
    contains the search term
    """
    template_name = 'teamSearch.html'
    form_class = TeamSearchForm

    def form_valid(self, form):
        """Lookup all teams whose name or tag contains the search term, then
        renders the search page with the teams found
        """
        search_name = form.cleaned_data['team_field']
        logger.info("Searching for teams whose name contains %s", search_name)
        teams = Team.objects.filter(
            Q(name__contains=search_name) |
            Q(tag__contains=search_name)
        )
        logger.info("Found the following teams: %s", teams)
        return render(self.request, "teamSearch.html", {'form': form, 'teams':
                                                        teams})


def team_page(request, team_name):
    """Renders the public profile of a user """
    logger.info("Rendering public team profile for team %s", team_name)
    team = Team.objects.get(name=team_name)
    members = TeamMembership.objects.filter(team=team)
    user_is_captain = is_captain(team, request.user)
    is_member = currently_a_member(team, request.user)
    join_form = JoinTeamForm()
    logger.info("Found team %s with members %s", team, members)
    return render(request, 'viewTeam.html', {'team': team, 'members': members,
                                             'is_captain': user_is_captain,
                                             'join_form': join_form,
                                             'is_member': is_member})


@login_required
def join_team(request, team_name):
    """Handles requests to join a team. Verifies that the user
    is not already on the team and has the right join password. Redirects
    to the team's home page with messages stating the outcome
    """
    logger.info(
        "Handling request for user %s to join team %s", request.user,
        team_name)
    team = Team.objects.get(name=team_name)
    if request.POST['join_password'] == team.join_password:
        logger.info("Join password was correct, adding user to team")
        if currently_a_member(team_name, request.user):
            logger.info("That user is already a member")
            messages.warning(
                request, "You are already a member of %s" % team_name)
        else:
            logger.info("User was not a member yet, adding to team")
            membership = TeamMembership(team=team, user=request.user)
            membership.save()
            messages.success(request,
                             "You successfully joined team %s!" % team_name)
    else:
        logger.info(
            "Join password %s did not match the team's join password",
            team.join_password)
        messages.error(request, "Join password was not correct. Please contact"
                                " the team captain if you do not know it")
    return redirect("/teams/%s" % team_name)


@login_required
def quit_team(request, team_name):
    """Handles a request to quit a team. Verifies that the user is on the team.
    Redirects to the team's home page with messages stating the outcome
    """
    logger.info(
        "Handling request for user %s to quit team %s", request.user,
        team_name)
    team = Team.objects.get(name=team_name)
    if currently_a_member(team, request.user):
        membership = TeamMembership.objects.get(team=team, user=request.user,
                                                active=True)
        membership.set_inactive()
        messages.success(request, "Successfully quit team %s" % team_name)
    else:
        logger.info("User was not a member of the team")
        messages.error(request, "You are not a member of team %s" % team_name)
    return redirect("/teams/%s" % team_name)


@login_required
def kick_player(request, team_name, username):
    """ Kicks the player with the passed in username from the team with the
    passed in team_name. Verifies that the user initiating this request is the
    team captain. Redirects to the team's home page with messages containing
    the result.
    """
    logger.info(
        "Handling request to kick %s from team %s", username, team_name)
    team = Team.objects.get(name=team_name)
    if not is_captain(team, request.user):
        logger.info(
            "User %s is not the captin of team %s, aborting request to kick"
            " player", request.user, team)
        messages.error(
            request, "You cannot kick players, you are not the captain")
    else:
        user = User.objects.get(username=username)
        if currently_a_member(team, user):
            logger.info("Kicking player from team")
            membership = TeamMembership.objects.get(
                user=user, active=True, team=team)
            membership.set_inactive()
            messages.success(request, "Successfully kicked %s" % username)
        else:
            logger.info("User %s is not currently on team %s", user, team)
            messages.error("User %s is not a member of that team" % user)
    return redirect("/teams/%s" % team_name)


@login_required
def promote_captain(request, team_name, username):
    """ Promotes the player with the passed in username to captain of the team
    with the passed in team_name. Verifies that the user initiating this
    request is the team captain. Redirects to the team's home page with
    messages containing the result.
    """
    logger.info(
        "Handling request to promote %s to captain of team %s", username,
        team_name)
    team = Team.objects.get(name=team_name)
    if not is_captain(team, request.user):
        logger.info(
            "User %s is not captain of %s, aborting request", request.user,
            team)
        messages.error(
            request, "You cannot promote players, you are not the captain")
    else:
        user = User.objects.get(username=username)
        if currently_a_member(team, user):
            logger.info("Promoting %s to captain of team %s", user, team)
            membership = TeamMembership.objects.get(
                user=user, active=True, team=team)
            membership.make_captain()
            messages.success(
                request, "Successfully promoted %s to captain" % username)
        else:
            logger.info(
                "User %s is not currently on team %s, so aborting making them"
                " captain", user, team)
            messages.error("User %s is not a member of that team" % user)
    return redirect("/teams/%s" % team_name)


def is_captain(team, user):
    """ Small helper function the returns true if the user is the captain of the
    passed in team, or false otherwise. If the user is not logged in then
    returns false
    """
    if not user.is_authenticated():
        return False
    memberships = TeamMembership.objects.filter(team=team, user=user,
                                                active=True, is_captain=True)
    return memberships.count() > 0


def currently_a_member(team, user):
    """ Small helper function the returns true if the user is a member of the
    passed in team, or false otherwise. If the user is not logged in then
    returns false
    """
    if not user.is_authenticated():
        return False
    memberships = TeamMembership.objects.filter(team=team, user=user,
                                                active=True)
    return memberships.count() > 0
