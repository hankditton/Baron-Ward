# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('teams', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='team',
            name='members',
            field=models.ManyToManyField(related_name='membership', through='teams.TeamMembership', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='teammembership',
            name='is_captain',
            field=models.BooleanField(default=False),
        ),
    ]
