# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
from django.conf import settings
import datetime


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Team',
            fields=[
                ('name', models.CharField(unique=True, serialize=False, max_length=50, primary_key=True)),
                ('tag', models.CharField(max_length=5)),
                ('join_password', models.CharField(max_length=12)),
                ('roster_locked', models.BooleanField(default=False)),
            ],
        ),
        migrations.CreateModel(
            name='TeamMembership',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('active', models.BooleanField(default=True)),
                ('join_date', models.DateField(default=datetime.datetime.now)),
                ('quit_date', models.DateField(null=True)),
                ('is_captain', models.BooleanField()),
                ('team', models.ForeignKey(to='teams.Team')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
        ),
        migrations.AddField(
            model_name='team',
            name='members',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='teams.TeamMembership'),
        ),
    ]
