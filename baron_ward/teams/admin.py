""" Admin settings for the teams app, allowing admins to edit teams and
membership
"""

from django.contrib import admin

from teams.models import TeamMembership, Team


# Register your models here.
class TeamMembershipInline(admin.TabularInline):

    """ Inline admin tool to see and edit TeamMembership entries
    from the team page
    """
    model = TeamMembership
    extra = 1


class TeamAdmin(admin.ModelAdmin):

    """ Custom admin model to add TeamMembershipInline
    """
    inlines = (TeamMembershipInline, )


admin.site.register(Team, TeamAdmin)
admin.site.register(TeamMembership)
