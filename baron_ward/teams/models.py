""" Models for team support, namely the Team model and the TeamMembership Model
"""
from datetime import datetime
import logging

from django.contrib.auth.models import User
from django.db import models


logger = logging.getLogger(__name__)


class Team(models.Model):

    """ Model for a team of players. Teams have a name, a team tag, members,
    and a join password used for players to join the team. A team's roster
    may be locked as well, disallowing any roster changes to occur.
    """
    name = models.CharField(max_length=50, unique=True, primary_key=True)
    tag = models.CharField(max_length=5)
    join_password = models.CharField(max_length=12)
    roster_locked = models.BooleanField(default=False)
    members = models.ManyToManyField(
        User, through='TeamMembership', related_name="membership")

    def __str__(self):
        return "Team: %s, tag: %s, locked: %s" % (self.name, self.tag,
                                                  self.roster_locked)


class TeamMembership(models.Model):

    """ Model to track additional information about a team membership, including
    if this is an active membership, the join and quit date, and if the user is
    currently the captain of the team
    """
    team = models.ForeignKey(Team)
    user = models.ForeignKey(User)
    active = models.BooleanField(default=True)
    join_date = models.DateField(default=datetime.now)
    quit_date = models.DateField(null=True)
    is_captain = models.BooleanField(default=False)

    def set_inactive(self):
        """ Sets the user to inactive, and sets the quit date to now then saves
        the model
        """
        logger.info(
            "Setting user % on team %s to no longer active", self.user,
            self.team)
        self.quit_date = datetime.now()
        self.active = False
        self.save()

    def make_captain(self):
        """ Sets this user to the current team captain and demotes the existing
        team captain
        """
        logger.info("Making user %s captain of team %s", self.user, self.team)
        current_captain = TeamMembership.objects.get(
            team=self.team, is_captain=True)
        current_captain.is_captain = False
        current_captain.save()
        self.is_captain = True
        self.save()

    def __str__(self):
        return "Team: %s, User: %s, Active: %s, Captain: %s" % (self.team.name,
                                                                self.user,
                                                                self.active,
                                                                self.is_captain
                                                                )
