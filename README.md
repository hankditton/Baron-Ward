# Baron-Ward
A website that hosts League of Legends tournaments, with a focus on providing tournaments that are gated by rating (e.g. only Gold and below rated players). Tournaments should run completely automatically if there are no issues.

##Features

- Users can create link accounts with their League of Legends account
- Users can create and manage teams
- Teams can sign up for tournaments, validating all players meet requirements
- During a tournament, bracket is automatically generated and displayed
- Tournament code and match results are automatically generated and displayed
- Provide a link to launch spectator client for live games

##Setup
- Requires Python 3
- Run "pip install -r requirements.txt" to install all required Python modules
- Copy settings_example.py to settings.py and fill out the required settings

This project's main goal is to learn more about Python, Django, and web development (Bootstrap, JS, etc.)

##Disclaimer
BaronWard isn't endorsed by Riot Games and doesn't reflect the views or opinions of Riot Games or anyone officially involved in producing or managing League of Legends. League of Legends and Riot Games are trademarks or registered trademarks of Riot Games, Inc. League of Legends © Riot Games, Inc.
